import re
import tty
import sys
import os
from requests_html import HTMLSession
from numpy import array
import requests
clear = lambda: system('clear')

def getLinklist(uinput):
    session = HTMLSession()
    resp = session.get("https://rutracker.org/forum/search_cse.php?q="+uinput)
    resp.html.render()
    pattern = re.compile(r'data-ctorig="([^"]*)">([^"]*)</a>')
    matches = re.findall(pattern, resp.html.html)

    i=0
    result = []
    for match in matches:
        i+=1
        if(i%2==1):
            result.append((match[0],match[1].replace('<b>','\033[1m').replace('</b>','\033[0m')))
    return result

def getContent(link):
    content = requests.get(link).text
    re.MULTILINE

    pattern = r'<div class="sp-head folded"><span>([^"]*)</span></div>\n<div.*</var>([^"]*)<span'
    matches = re.findall(pattern, content)
    contentResult = []
    for match in matches:
        contentResult.append((match[0].replace('&#58;',':').replace('&#41;',')').replace('&#91;','[').replace('&#93;',']'),match[1].replace('<br>','')))

    #---Size
    size = re.findall(r'<li>([^"]*)</li>',content)
    sizeformat = "N/A"
    magnet=False
    if size != []:
        sizeformat = size[0].replace('&nbsp;',' ')
        #---Link
        magnet = re.findall(r'<a href="(magnet[^"]*)" class',content)[0]
        #---Link End
    #---Size End

    return contentResult, sizeformat, magnet
def showContent(link):
    x = 0
    select = 0
    contentResult, size, magnet = getContent(link)
    print(contentResult)
    unfolded = [False] * len(contentResult)
    while x != 'h': # ESC
        rows, columns = os.popen('stty size', 'r').read().split()
        print(int(rows)*'\n')
        enumber=0
        for album in contentResult:
            if(enumber==select):
                print('\033[4m\033[1m',album[0],'\033[0m',sep='')
            else:
                print('\033[1m',album[0],'\033[0m',sep='')
            if unfolded[enumber] == True:
                print(album[1])
                #for song in album[1]:
                #    print('   ',song)
            enumber+=1
        print(size)
        x=sys.stdin.read(1)[0]
        if x == 'j':
            if select+1 < len(contentResult):
                select+=1
        if x == 'k':
            if select > 0:
                select-=1
        if x == 'l':
            if unfolded[select] == False:
                unfolded[select] = True
            else:
                unfolded[select] = False
        if x == chr(10):
            if magnet != False:
                os.system('qbittorrent "{}"'.format(magnet))
                quit()

        if x == 'q':
            quit()
            
    
def main():
    uinput = input("Search : ").lower()

    result = getLinklist(uinput)
    magnet = []
    data=[]
    for element in result:
        data.append((getContent(element[0])[1],getContent(element[0])[2]))

    tty.setcbreak(sys.stdin)
    x = 0
    select = 0
    while x != 'q': # ESC
        rows, columns = os.popen('stty size', 'r').read().split()
        print(int(rows)*'\n')
        enumber=0
        for element in result:
            print(data[enumber][0],'\t',sep='',end='')
            if(enumber==select):
                print('\033[4m',element[1].replace('\033[0m',''),'\033[0m',sep='')
            else:
                print(element[1])
            enumber+=1
        x=sys.stdin.read(1)[0]
        if x == 'j':
            if select+1 < len(result):
                select+=1
        if x == 'k':
            if select > 0:
                select-=1
        if x == 'l':
            print(80*'\n')
            showContent(result[select][0])
        if x == chr(10):
            if data[select][1] != False:
                os.system('qbittorrent "{}"'.format(data[select][1]))
                quit()
main()




