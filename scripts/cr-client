#!/bin/python

import re
import os
import subprocess
import cloudscraper #needed to pass cloudflare

LANG="frFR"
CACHE=os.environ['HOME']+"/.cache/cr-client/"
LIST=CACHE+"list.txt"
#UPDATE_ICON_PATH="/usr/share/icons/Adwaita/64x64/emblems/emblem-synchronizing-symbolic.symbolic.png"
UPDATE_ICON_PATH="/usr/share/icons/breeze-dark/emblems/24/vcs-update-required.svg"


scraper = cloudscraper.create_scraper()
series_list={}


def callRofi(mesg,options):
    ps = subprocess.Popen(('echo', '-en', options), stdout=subprocess.PIPE)
    ret = subprocess.check_output(['rofi','-dmenu', '-markup-rows', '-theme', '~/.config/rofi/themes/cr-client.rasi', '-sep', '\\x0f', '-i', '-matching', 'normal', '-p', 'CR-client', '-mesg', mesg], stdin=ps.stdout)
    return ret

def notify(title, msg):
    subprocess.Popen(('dunstify', '-a', 'CR-client', '-t', '4000', title, msg))
    return
def notifyThumbnail(title, msg, thumbnail):
    subprocess.Popen(('dunstify', '-a', 'CR-client', '-t', '4000', title, msg, '--icon={}'.format(thumbnail)))
    return


def createThumbnail(html, thumbnail_path):
    global scraper
    pattern_thumbnail = re.compile(r' <meta property="og:image" content="([^"]*)"/>')
    thumbnail_match = re.findall(pattern_thumbnail, html)
    #thumbnail_match = re.match(pattern_thumbnail, html)

    if not os.path.exists(thumbnail_path):
        thumbnail = scraper.get(thumbnail_match[0])
        with open(thumbnail_path, 'wb') as fp:
            fp.write(thumbnail.content)

 
def episodeSearch(selected):
    global scraper
    global series_list
    episode_list = {}
    html = scraper.get("https://www.crunchyroll.com"+series_list.get(selected)).text
    pattern = re.compile(r'<a href="([^"]*)" title="([^"]*)"')
    matches = re.findall(pattern, html)

    thumbnail_path = CACHE+series_list.get(selected)[1:]+".jpg"
    options = ""
    for match in matches:
        episode_list[match[1]] = match[0]
        if(len(match[1])>40):
            option=""
            size=0
            for word in match[1].split(' '):
                if(size+len(word)<45):
                    option+=word+' '
                    size+=len(word)+1
                else:
                    option+='\n'+word+' '
                    size=len(word)+1
        else:
            option = match[1] + ' '

        options += option+"\\0icon\\x1f"+thumbnail_path+'\\x0f'
    try:
        episode = callRofi("Chose episode", options[:-1])[:-2].decode("utf-8").replace('\n','')
    except:
        quit()
    notifyThumbnail("Playing Video", episode, thumbnail_path)
    #print(episode)
    #print(episode_list.get(episode))
    try:
        subprocess.check_output(('mpv', '--slang={}'.format(LANG), "http://www.crunchyroll.com"+episode_list.get(episode)))
    except:
        notify("Error", "An error happened while playing the video")

def updateList():
    global scraper
    notify("Updating cache", "We are updating the local list and thumbnails. If it is the first time it can take a while")
    try:
        html = scraper.get("https://www.crunchyroll.com/videos/anime/alpha?group=all").text
    except:
        notify("Error", "The scraper fail to pass the captcha")
        return

    pattern = re.compile(r'<a title="([^"]*)" token="shows-portraits" itemprop="url" href="([^"]*)" class="text-link ellipsis">')
    matches = re.findall(pattern, html)
    output_list=[]
    for match in matches:
        output_list.append("{}$$${}\n".format(match[0], match[1]))

    with open(LIST, 'r') as fp:
        file_content = fp.read()
    for item in output_list:
        if item not in file_content:
            createThumbnail(scraper.get("https://www.crunchyroll.com"+item.split('$$$')[1][:-1]).text, CACHE+item.split('$$$')[1][1:-1]+".jpg")
            notifyThumbnail("Thumbnail added", item.split('$$$')[0], CACHE+item.split('$$$')[1][1:-1]+".jpg")
            #print(item.split('$$$')[1][1:-1]+".jpg")

    with open(LIST, 'w') as fp:
        fp.writelines(output_list)



    return

def seriesSearch():
    global series_list
    #options = "--- Update ---\\0icon\\x1f"+UPDATE_ICON_PATH+'\n'
    options = "--- Update ---\\0icon\\x1f"+UPDATE_ICON_PATH+'\\x0f'
    with open(LIST, 'r') as fp:
        for line in fp:
            match = line.split('$$$')
            series_list[match[0]]=match[1][:-1]
            thumbnail_path = CACHE+match[1][1:-1]+".jpg"

            #options+=match[0]+"\\0icon\\x1f"+thumbnail_path+'\n'
            options+=match[0]+"\\0icon\\x1f"+thumbnail_path+'\\x0f'
    try:
        selected = callRofi("Chose series", options[:-1])[:-1].decode("utf-8") 
    except:
        quit()
    if(selected=="--- Update ---"):
        return 1
    episodeSearch(selected)
    return

if not os.path.exists(CACHE):
    os.makedirs(CACHE)
    updateList()
if not os.path.exists(LIST):
    updateList()

while(seriesSearch()==1):
    updateList()

quit()
