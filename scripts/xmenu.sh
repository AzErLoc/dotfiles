#!/bin/sh
music=$(mpc -f "%title% / %artist% / %album%" | head -n 1)
TERMINAL="urxvt"
EDITOR="nvim"

cat <<EOF | xmenu -p 0x30 | sh &
  Applications
	IMG:/usr/share/pixmaps/brave.png	Brave	brave
	Calcurse	$TERMINAL -e calcurse
	IMG:/usr/share/icons/hicolor/24x24/apps/chromium.png	Chromium	chromium	
	IMG:/usr/share/pixmaps/discord.png	Discord	discord
	IMG:/usr/share/pixmaps/qbittorrent.png	qBittorrent	qbittorrent
	Thunar	thunar
  Games
	IMG:$HOME/Games/factorio/data/core/graphics/factorio.ico	Factorio	lutris lutris:rungameid/2
	IMG:$HOME/.local/share/icons/hicolor/128x128/apps/lutris_osu.png	Osu	lutris lutris:rungameid/1
	
	IMG:/usr/share/icons/hicolor/24x24/apps/lutris.png	Lutris	lutris
	IMG:/usr/share/pixmaps/steam.png	Steam	steam
  Graphics
	IMG:/usr/share/icons/hicolor/24x24/apps/gimp.png	GIMP	gimp
  Music
	Ncmpcpp	~/scripts/music
	
	$music	~/scripts/music
	Discord-rpc	python ~/scripts/discord-rpc/example.py

  Edit Configs
	Bspwm	$TERMINAL -e $EDITOR /home/az/.config/bspwm/bspwmrc
	BspNoSwallow	$TERMINAL -e $EDITOR ~/.config/bspwm/noswallow
	Dunst	$TERMINAL -e $EDITOR ~/.config/dunst/dunstrc
	Ncmpcpp	$TERMINAL -e $EDITOR ~/.ncmpcpp/config
	 Nvim	$TERMINAL -e $EDITOR ~/.config/nvim/init.vim
	Picom	$TERMINAL -e $EDITOR ~/scripts/ressources/picom.conf
	Polybar	$TERMINAL -e $EDITOR ~/.config/polybar/config
	Startup	$TERMINAL -e $EDITOR ~/scripts/startup
	Sxhkd	$TERMINAL -e $EDITOR ~/.config/sxhkd/sxhkdrc
	Xmenu	$TERMINAL -e $EDITOR ~/scripts/xmenu.sh
襁  Settings
	  LX Appearance	bspc rule -a '*' -o state=floating && lxappearance
	蠟  Wallpapers	bspc rule -a '*' -o state=floating && sxiv -tbo -s f -N "Wallpapers" ~/Pictures/Wallpapers | xargs ~/scripts/wallpaper
	廊  Live Wallpapers	~/scripts/sxivlivewall
  Wireless Network
	  Bluetooth	~/.config/rofi/scripts/rofi-bluetooth
	直  Wifi	~/.config/rofi/scripts/nmcli-rofi
	
	ﯱ  Network Manager	bspc rule -a '*' -o state=floating && nm-connection-editor
  Keyboard : $(cat ~/scripts/ressources/key)	~/scripts/keyboard
  Take Screenshot	flameshot gui

  Terminal	$TERMINAL

  Power Menu	~/scripts/power
EOF
