# dotfiles

My dotfiles

Config :
* **WM** : bspwm
* **Bar** : polybar
* **Compositor** : picom-ibhagwan
* **File Managers** : ranger & Thunar
* **Font Terminal** : Inconsolata Nerd Font
* **GTK Theme** : Sweet-Dark
* **Icons** : Sweet-Purple-Filled on top of Breeze-Dark
* **Lock** : i3lock-colors
* **Launcher** : rofi
* **Menu** : xmenu
* **Music Player** : mpd with ncmpcpp
* **Terminal** : urxvt


For information I use **doas** instead of **sudo**. 
To replace **doas** with **sudo** in **~/scripts/** and **~/.config/polybar/config** easily do : 
    `cd scripts && grep -RiIl 'doas' | xargs sed -i 's/doas/sudo/g'`
    `cd .config/polybar && grep -RiIl 'doas' | xargs sed -i 's/doas/sudo/g'`
